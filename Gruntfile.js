module.exports = function(grunt) {


    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            options: {
                stripBanners: true,
                banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
                '<%= grunt.template.today("yyyy-mm-dd") %> */',
            },
            dist: {
                src: ['js/**/*.js'],
                dest: 'js/dist/built.js'
            }
        },

        less: {
            development: {
                options: {
                    path:['less/']
                },
                files: {
                    'css/calc.css' : 'less/calc.less'
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-less');
};