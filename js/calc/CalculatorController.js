(function () {
    'use strict';

    angular.module('calculatorApp').controller('CalculatorController', ['$scope', 'calculatorService', 'operatorService', 'digitsService',
        function ($scope, calculatorService, operatorService, digitsService) {
        var DEFAULT_VALUE = 0;
        var EQUAL_OPERATOR = '=';
        var firstNumber = DEFAULT_VALUE;
        var secondNumber = null;

        $scope.firstNumber = function () {
            return String(firstNumber);
        };

        $scope.secondNumber = function () {
            return secondNumber == null ? '' : String(secondNumber);
        };

        $scope.operator = null;

        $scope.numbers = digitsService.get();
        $scope.operators = operatorService.get();


        $scope.btnNumberClick = function(number) {
            if (!$scope.operator) {
                firstNumber = firstNumber * 10 + number;
                secondNumber = null;
            } else {
                secondNumber = secondNumber * 10 + number;
            }
        };

        $scope.btnOperatorClick = function(operator){
            if (operator === EQUAL_OPERATOR && secondNumber) {
                firstNumber = calculatorService.evalResult($scope.operator, firstNumber, secondNumber);
                secondNumber = null;
                $scope.operator = null;
            } else {
                $scope.operator = operator;
            }
        };
    }]);

})();