(function () {
    'use strict';

    angular.module('calculatorApp').factory('operatorService', function(){
        return {
            get: function() {
                return ['/', '*', '-', '+', 'c', '='];
            }
        }
    });

})();