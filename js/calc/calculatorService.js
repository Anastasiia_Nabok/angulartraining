(function () {
    'use strict';

    angular.module('calculatorApp').factory('calculatorService', function(){
        var evalResult = function (operator, firstNum, secondNum) {
            switch (operator) {
                case '+':
                    return firstNum + secondNum;
                    break;
                case '-':
                    return firstNum - secondNum;
                    break;
                case '*':
                    return firstNum * secondNum;
                    break;
                case '/':
                    return firstNum / secondNum;
                    break;
            }
        };

        return {
            evalResult: evalResult
        }
    });

})();