(function () {
    angular.module('calculatorApp').directive('calculator', function(){
        return {
            restrict: 'E',
            templateUrl: 'views\calculator.html'
        }
    });

})();