(function () {
    'use strict';

    angular.module('calculatorApp').factory('digitsService', function(){
        return {
            get: function() {
                return [9, 8, 7, 6, 5, 4, 3, 2, 1, 0];
            }
        }
    });

})();